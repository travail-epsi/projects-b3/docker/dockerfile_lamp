FROM debian:10

RUN apt update && apt install -y php \
          php-fpm \
          nginx \
          mariadb-client \
          mariadb-server

WORKDIR /usr/local/lamp

COPY inventaire.sql /usr/local/lamp/inventaire.sql
COPY site.conf /etc/nginx/site-enabled/default
COPY php.ini /etc/php/php.ini

EXPOSE 80/tcp
EXPOSE 3306/tcp

CMD ["bash", "-c", "service php7.3-fpm start && service mysql start && service nginx start && mysql < /usr/local/lamp/inventaire.sql && tail -f /dev/null"]
